<?php

namespace Drupal\twitter_filter\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to convert Twitter #hashtags and @usernames into links.
 *
 * @Filter(
 *   id = "twitter_filter",
 *   title = @Translation("Twitter filter"),
 *   description = @Translation("Convert Twitter #hashtags and @usernames into links"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "link_hashtags" = "hashtag_page",
 *     "link_usernames" = "user_page"
 *   }
 * )
 */
class TwitterFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['link_hashtags'] = [
      '#type' => 'select',
      '#title' => $this->t('Link #hashtags to'),
      '#options' => [
        'hashtag_page' => $this->t('Twitter hashtag page'),
        'search_page' => $this->t('Twitter search page'),
      ],
      '#default_value' => $this->settings['link_hashtags'],
    ];

    $form['link_hashtags_target'] = [
      '#type' => 'select',
      '#title' => $this->t('#hashtags link open in new tab'),
      '#options' => [
        'none' => $this->t('No'),
        '_blank' => $this->t('Yes'),
      ],
      '#default_value' => isset($this->settings['link_hashtags_target']) ? $this->settings['link_hashtags_target'] : 'none',
    ];

    $form['link_usernames'] = [
      '#type' => 'select',
      '#title' => $this->t('Link @usernames to'),
      '#options' => [
        'user_page' => $this->t('Twitter user page'),
        'search_page' => $this->t('Twitter search page'),
      ],
      '#default_value' => $this->settings['link_usernames'],
    ];

    $form['link_usernames_target'] = [
      '#type' => 'select',
      '#title' => $this->t('@usernames link open in new tab'),
      '#options' => [
        'none' => $this->t('No'),
        '_blank' => $this->t('Yes'),
      ],
      '#default_value' => isset($this->settings['link_usernames_target']) ? $this->settings['link_usernames_target'] : 'none',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Twitter #hashtags and @usernames turn into links automatically.');
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $text = self::processHashtag($text, $this->settings['link_hashtags'], $this->settings['link_hashtags_target']);
    $text = self::processUsername($text, $this->settings['link_usernames'], $this->settings['link_usernames_target']);

    return new FilterProcessResult($text);
  }

  /**
   * Convert Twitter #hashtags into links.
   */
  public static function processHashtag($text, $link_hashtags, $target_options) {
    $target = '';
    if ('_blank' === $target_options) {
      $target = ' target="_blank"';
    }

    if ('search_page' === $link_hashtags) {
      return preg_replace('/(^|\s)#(\w*[\p{M}\p{L}]+[\p{M}\p{L}]*)/u', '\1<a class="twitter-hashtag"' . $target . ' href="https://twitter.com/search?q=%23\2">#\2</a>', (string) $text);
    }

    return preg_replace('/(^|\s)#(\w*[\p{M}\p{L}]+[\p{M}\p{L}]*)/u', '\1<a class="twitter-hashtag"' . $target . ' href="https://twitter.com/hashtag/\2">#\2</a>', (string) $text);
  }

  /**
   * Converts Twitter @usernames into links.
   */
  public static function processUsername($text, $link_usernames, $target_options) {
    $target = '';
    if ('_blank' === $target_options) {
      $target = ' target="_blank"';
    }

    if ('search_page' === $link_usernames) {
      return preg_replace('/(^|\s)@(\w*[\p{M}\p{L}]+[\p{M}\p{L}]*)/u', '\1<a class="twitter-username"' . $target . ' href="https://twitter.com/search?q=%40\2">@\2</a>', (string) $text);
    }

    return preg_replace('/(^|\s)@(\w*[\p{M}\p{L}]+[\p{M}\p{L}]*)/u', '\1<a class="twitter-username"' . $target . ' href="https://twitter.com/\2">@\2</a>', (string) $text);
  }

}
